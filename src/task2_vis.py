from comet_ml import Experiment
import numpy as np
import dlipr
from tensorflow import keras

models = keras.models
layers = keras.layers
KTF = keras.backend

# Set up YOUR experiment - login to comet, create new project (for new exercise)
# and copy the statet command
# or just change the name of the workspace, and the API (you can find it in the settings)
experiment = Experiment(api_key="EnterYourAPIKey",
                        project_name="exercise6", workspace="EnterGroupWorkspaceHere")

# ----------------------------------------------
# Data
# ----------------------------------------------

data = dlipr.mnist.load_data()

# plot some examples
data.plot_examples(fname='examples.png')

# reshape the image matrices
X_train = data.train_images.reshape(data.train_images.shape[0], 28, 28, 1)
X_test = data.test_images.reshape(data.test_images.shape[0], 28, 28, 1)
print('%i training samples' % X_train.shape[0])
print('%i test samples' % X_test.shape[0])

# Preprocess data : convert integer RGB values (0-255) to float values (0-1)
X_train = X_train.astype('float32') / 255.
X_test = X_test.astype('float32') / 255.

# convert class labels to one-hot encodings
Y_train = keras.utils.to_categorical(data.train_labels, 10)
Y_test = keras.utils.to_categorical(data.test_labels, 10)


# ----------------------------------------------
# Model and training
# ----------------------------------------------

model = models.Sequential([])
# Set up a convolutional network with at least 3 convolutional layers
# and train the network to at least 98% test accuracy
#
# model.compile()
#
# results = model.fit(X_train, Y_train,
#     validation_split=0.1,  # split off 10% of training data for validation
#     )
#
# model.save("mnist_model.h5") # save your model
# model = models.load_model("mnist_model.h5")  # load your trained model -> you only have to train the model once!
# model.summary()

# ----------------------------------------------
# Visualization
# ----------------------------------------------


def deprocess(x):
    '''Use this function before plotting the visualized feature map'''
    # standard normalize the tensor
    x -= x.mean()
    x /= (x.std() + KTF.epsilon())
    x *= 0.1

    # clip values [0, 1] and convert back to RGB
    x += 0.5
    x = np.clip(x, 0, 1)
    x *= 255
    x = np.clip(x, 0, 255).astype('uint8')
    return x


def normalize(x):
    '''Normalize the gradients via the l2 norm'''
    return x/(KTF.sqrt(KTF.mean(KTF.square(x))) + KTF.epsilon())


layer_dict = dict([(layer.name, layer) for layer in model.layers[:]])
layer_names = []  # build array of layer_names you would like to visualize
gradient_updates = 50
step_size = 1.


for layer_name in layer_names:

    print("process layer", layer_name)
    layer_output = layer_dict[layer_name].output
    visualized_filter = []
    # build submodel which has as input the model input and as output the stack of feature maps of a specific layer
    sub_model = models.Model([model.inputs], [layer_output])

    for filter_index in range(layer_output.shape[-1]):  # iterate over fiters

        print('Processing filter %d' % (filter_index+1))

        # Start from uniform distributed noise images (Note: shape has to be (1, 28, 28, 1), as we use 1 as batchsize)
        # Now choose one specific feature map using 'filter_index'
        # Then create a scalar loss as discussed in the lecture (maximize the average activation of the feature map)
        # Further calculate the gradient of the loss w.r.t. to the input (our noise image / or rather the noise variable)
        # Afterwards, add the calculated gradients to your start image (gradient ascent step) and repeat the procedure
        # using gradient_updates = 50
        
        # You can calculate the gradients using the following expressions:
        # with tf.GradientTape() as gtape:
        #   
        #    grads = gtape.gradient(YOUR_OBJECTIVE, THE_VARIABLE_YOU_WANT_TO_OPTIMIZES)
        #    grads = normalize(grads)
        #    # Then implement Gradient ascent step: you may use the assign_sub or assign_add of adaptive variables


        # ######################################################################### 
        # The following code snippet may help you to implement the maximization 
        # #########################################################################
        
        # Construct keras variable for input (we want to find an input which maximizes the ouput, so we build a input which 
        # holds adaptive parameters which we can train using tensorflow / keras)        
        input_img = KTF.variable([0]) # Instead '[0]' use noise as start (init) for the input image
        
        for i in range(gradient_updates):
            
            with tf.GradientTape() as gtape:
                # define a scalar loss using Keras.
                # remember: You would like to maximize the activations in the respective feature map!
                loss = 0  # <--: define your loss HERE



        # We need to use deprocess() before plotting the visualized feature map
        visualized_filter.append(deprocess_image(input_img.numpy()))  # cast to numpy array

        # Finally plot the visualized filter and comment on the patterns you observe
        # DON'T FORGET TO LOG THE IMAGES USING COMET: experiment.log_figure(figure=plt)
        # Did the visualization work for all feature maps?        




