from comet_ml import Experiment
import numpy as np
import matplotlib.pyplot as plt
import dlipr
from tensorflow import keras
from PIL import Image
from tensorflow.keras.layers import Input
from tensorflow.keras.applications.resnet50 import preprocess_input, decode_predictions
inc = keras.applications.inception_v3
xce = keras.applications.xception


experiment = Experiment(api_key="EnterYourAPIKey",
                        project_name="exercise6", workspace="irratzo")

# ------------------------------------------------------------
# Apply a trained ImageNet classification network to classify new images.
# See https://keras.io/applications/ for further instructions.
# ------------------------------------------------------------

# VISPA Note: Keras downloads the pretrained network weights to ~/.keras/models.
# To save space in your home folder you can use the /net/scratch/deeplearning/keras-models folder.
# Simply open the terminal and copy/paste:
# ln -s /net/scratch/deeplearning/keras-models ~/.keras/models
# If you get an error "cannot overwrite directory", remove the existing .keras/models folder first.
# Alternatively, you can set up the model with "weights=None" and then use model.load_weights('/net/scratch/deeplearning/keras-models/...')


im1 = np.array(Image.open('1.jpg'))
im2 = np.array(Image.open('2.jpg'))
im3 = np.array(Image.open('3.jpg'))
im4 = np.array(Image.open('4.jpg'))
im = np.array([im1,im2,im3,im4])

for i in range(im.shape[0]):
    plt.figure(i)
    plt.imshow(im[i])
    plt.savefig(str(i)+"replot.png")

print("Inception-V3:")
model = inc.InceptionV3(weights='imagenet')

predictions=model.predict(im)

predictions1 = decode_predictions(predictions, top=10)



print("Xception:")
model = xce.Xception(weights='imagenet')

predictions=model.predict(im)

predictions2 = decode_predictions(predictions, top=10)
for i in range(len(im)):
    print(i+1)
    for j in range(10):
        print("|",predictions1[i][j][1],predictions1[i][j][2], "|", predictions2[i][j][1],predictions2[i][j][2],"|")
        
        
        
        
        

