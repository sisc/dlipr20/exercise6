#Correction
##Task 1
###1)
3/3 P
###2)
2/2 P
##Task 2
0/9 P
##Task 3
0/11 P
#Total 5/25 P





**RWTH Aachen - Deep Learning in Physics Research SS2020**

**Exercise 6 Advanced Computer Vision Methods**

Date: 08.06.2020

Students:
| student            | rwth ID |
|--------------------|---------|
| Baptiste Corneglio | 411835|
| Florian Stadtmann  | 367319 |
| Johannes Wasmer    |  090800 |


*Task 1*

After downloading the Inception V3 network and the Xception network, two images are inserted for classification. Random images were chose, that are not images the network is trained on. Below the images, the top 10 predicitons and their values are given.

![](https://www.comet.ml/api/image/notes/download?imageId=lPewl0CWX6ElfDzwuLfOP3hW0&amp;objectId=58ceb5df64a84e0f8ec6093fd180c7ab)

| Inception V3 | Xception |
|--------------------------------|----------------------------|
| pitcher 0.9998938 | crib 0.9997708 |
| saltshaker 0.000106183244 | bulletproof_vest 0.00021087095 |
| water_jug 8.071678e-13 | military_uniform 1.2561688e-05 |
| fountain 3.5318717e-19 | magnetic_compass 3.1496686e-06 |
| water_bottle 1.46274e-21 | nipple 1.3251947e-06 |
| vase 1.31242134e-23 | amphibian 7.836647e-07 |
| Persian_cat 1.24524234e-23 | web_site 3.8438318e-07 |
| harvester 6.935049e-24 | backpack 8.9856464e-08 |
| jigsaw_puzzle 6.332574e-24 | scoreboard 1.3668058e-08 |
| church 8.296902e-25 | sunglasses 7.239693e-09 |


![](https://www.comet.ml/api/image/notes/download?imageId=5vBbZmRIGceInLYNh9InbSNZn&amp;objectId=58ceb5df64a84e0f8ec6093fd180c7ab)

| Inception V3 | Xception |
|--------------------------------|----------------------------|
| pitcher 0.98725617 | mixing_bowl 0.993421 |
| saltshaker 0.012731563 | keeshond 0.006423159 |
| harvester 1.2243946e-05 | ping-pong_ball 0.00013096459 |
| jigsaw_puzzle 3.5030025e-13 | golf_ball 1.9468154e-05 |
| rocking_chair 1.7681447e-14 | table_lamp 2.8560564e-06 |
| fountain 3.5610263e-16 | carton 2.3731793e-06 |
| church 2.4570507e-18 | envelope 1.3322229e-07 |
| brown_bear 2.1375418e-18 | sunglasses 2.5007486e-08 |
| bullfrog 2.0254367e-18 | Norwegian_elkhound 7.464001e-09 |
| kimono 1.7975538e-19 | crib 4.0052632e-09 |


The predictions are very unexpected. Inception V3 predicts big values for pitchers and salt shakers, which do not fit.
The Xception network predicts cribs and micing bowls.


Now images are tested that depict objects that the network has definitly a classification class of:

![](https://www.comet.ml/api/image/notes/download?imageId=VNHAsGzjpGOsiPh6N5dFgUwTx&amp;objectId=58ceb5df64a84e0f8ec6093fd180c7ab)
| Inception V3 | Xception |
|--------------------------------|----------------------------|
| pitcher 1.0 | mixing_bowl 1.0 |
| flatworm 7.462745e-20 | safety_pin 4.9210934e-26 |
| saltshaker 1.9043187e-20 | ping-pong_ball 1.1886899e-37 |
| clog 2.0857744e-33 | toilet_tissue 0.0 |
| vase 1.7851195e-33 | zebra 0.0 |
| sorrel 0.0 | guinea_pig 0.0 |
| guinea_pig 0.0 | beaver 0.0 |
| beaver 0.0 | marmot 0.0 |
| marmot 0.0 | fox_squirrel 0.0 |
| fox_squirrel 0.0 | porcupine 0.0 |

![](https://www.comet.ml/api/image/notes/download?imageId=RDgifSAHr6Of9jb3z9PWOVaQ9&amp;objectId=58ceb5df64a84e0f8ec6093fd180c7ab)
| Inception V3 | Xception |
|--------------------------------|----------------------------|
| web_site 1.0 | mixing_bowl 1.0 |
| flatworm 5.033761e-33 | safety_pin 2.7818835e-08 |
| pitcher 5.596241e-38 | toilet_tissue 0.0 |
| binoculars 2.514162e-38 | hog 0.0 |
| toilet_tissue 0.0 | sorrel 0.0 |
| hamster 0.0 | guinea_pig 0.0 |
| hare 0.0 | beaver 0.0 |
| Angora 0.0 | marmot 0.0 |
| porcupine 0.0 | fox_squirrel 0.0 |
| sea_cucumber 0.0 | porcupine 0.0 |

Again, the predictions are completely off. All pictures are plotted again to make shure that nothing went wrong after the read in process, but they look the same before and after, exept for a little bit lower resolution:

![](https://www.comet.ml/api/image/notes/download?imageId=ovKZMSCDvhxB6EedNA0VWmqoK&amp;objectId=58ceb5df64a84e0f8ec6093fd180c7ab)
![](https://www.comet.ml/api/image/notes/download?imageId=4wT47Depa9Qzkgt9tPEyUwIFK&amp;objectId=58ceb5df64a84e0f8ec6093fd180c7ab)
![](https://www.comet.ml/api/image/notes/download?imageId=GlD65ukLwNbt5qzCbR1vi0PMb&amp;objectId=58ceb5df64a84e0f8ec6093fd180c7ab)
![](https://www.comet.ml/api/image/notes/download?imageId=dYBkEdvPJgZir4fmw4lgZJVT2&amp;objectId=58ceb5df64a84e0f8ec6093fd180c7ab)
